package exercises;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Ex2Parameters {
    @BeforeAll
    public static void BeforeTests() {
        RestAssured.baseURI="http://localhost";
        RestAssured.port=3000;

    }

    /*******************************************************
     * Check that the user with ID 1 has 20 todos
     * Use pathParam for creating the request
     ******************************************************/
    @Test
    public void testTodosForUser() {
        given().queryParam("userId","1").when().get("/todos").then().assertThat().body("size()",is(20));


    }


    /*******************************************************
     * Check that the user with ID 1 has a post called "qui est esse"
     * Use queryParam for creating the request
     ******************************************************/
    @Test
    public void testPostForUser() {
        given().log().all().queryParam("userId","1").when().get("/posts").then().log().all().assertThat().body("title",hasItem("qui est esse"));
    }
}