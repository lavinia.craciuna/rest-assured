package exercises;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import model.Post;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class Ex5ObjectModeling {
    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;

    /*************************************************************
     * Request specification should include our base URI
     * and base path pointing to posts collection
     ************************************************************/
    @BeforeAll
    public static void createSpecifications() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("http://localhost");
        builder.setPort(3000);
        builder.setBasePath("/posts");
        requestSpec=builder.build();
        responseSpec=new ResponseSpecBuilder().expectStatusCode(201).expectContentType(ContentType.JSON).build();

    }

    /**********************************************************
     * Check the title of any post using deserialization.
     * You should create a new class Post.java that has the same
     * structure as the objects in the posts collection and
     * has a getter method for the title property.
     *********************************************************/
    @Test
    public void testGetPost() {
        Post myPost=given().log().all().spec(requestSpec).pathParam("id","2").get("/{id}").as(Post.class);
        assertEquals(myPost.getTitle(),"qui est esse");




    }

    /**********************************************************
     * Add a new item to posts collection using serialization.
     * You should use the new class Post.java - create a new
     * instance of it and set it as the request body.
     * Make sure your Post.java class has a constructor.
     * Check that the response status code is 201 (created).
     *********************************************************/
    @Test
    public void testAddPost() {
        Post addPost=new Post (10,103,"titlu","body");
        given().spec(requestSpec).body(addPost).contentType("application/json").when().post().then().log().all().assertThat().spec(responseSpec);



    }
}
