package exercises;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class Ex3Specifications {

    private static RequestSpecification requestSpec;
    private static ResponseSpecification responseSpec;

    /*************************************************************
     * Create request and response specifications using builders
     * Request specification should include our base URI and
     * a base path that points to comments collection
     * Response specification should expect status code 200
     ************************************************************/
    @BeforeAll
    public static void createSpecifications() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("http://localhost");
        builder.setPort(3000);
        builder.setBasePath("/comments");
        requestSpec=builder.build();
        responseSpec=new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();
    }

    /**********************************************************
     * Delete the comment with ID 1 (use both specifications)
     *********************************************************/
    @Test
    public void testDeleteComment() {
        given().log().all().spec(requestSpec)
                .pathParam("id","1").delete("/{id}").then().log().all().spec(responseSpec);

    }

    /**********************************************************
     * Update the comment with ID 2
     * (use both specifications)
     *********************************************************/
    @Test
    public void testAddComment() {
        given().log().all().spec(requestSpec)
                .pathParam("id","2").body("   {\n" +
                "    \"postId\": 1,\n" +
                "    \"id\": 2,\n" +
                "    \"name\": \"NUMELE\",\n" +
                "    \"email\": \"ADRESA DE EMAIL\",\n" +
                "    \"body\": \"ALTCEVA\"\n" +
                "  }").header("content-Type","application/json").put("/{id}").then().log().all().spec(responseSpec);

    }
}