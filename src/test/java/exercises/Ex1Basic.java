package exercises;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;


public class Ex1Basic {
    @BeforeAll
    public static void BeforeTests() {
        RestAssured.baseURI="http://localhost";
        RestAssured.port=3000;

    }

    /*******************************************************
     * Send a GET request to "  http://localhost:3000/users"
     * and check that the response has HTTP status code 200
     ******************************************************/
    @Test
    public void testGetUsers() {
      //  given().when().get("http://localhost:3000/users").then().assertThat().statusCode(200);
        given().when().get("/users").then().assertThat().statusCode(200);

    }


    /*******************************************************
     * Send a GET request to ""  http://localhost:3000/incorrect"
     * and check that the answer has HTTP status code 404
     ******************************************************/
    @Test
    public void testIncorrectRequest() {
        given().when().get("http://localhost:3000/incorrect").then().assertThat().statusCode(404);

    }


    /*******************************************************
     * Retrieve users collection and assert that
     * the returned content type is "application/json"
     * and the collection has 10 items
     ******************************************************/
    @Test
    public void testUsersSize() {
        given().when().get("http://localhost:3000/users").then().assertThat().contentType("application/json").and().assertThat().body("size()", is(10));
        given().when().get("http://localhost:3000/users").then().assertThat().contentType("application/json").and().assertThat().body("id", hasSize(10));


    }


    /*******************************************************
     * Check that the users collection contains items
     * having company name "Johns Group"
     ******************************************************/
    @Test
    public void testUsersWithCompany() {
        given().when().get("http://localhost:3000/users").then().assertThat().contentType("application/json").and().assertThat().body("company.name", hasItem("Johns Group"));
    }

}
